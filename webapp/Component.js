sap.ui.define([
	"sap/ui/core/UIComponent",
	"sap/ui/Device",
	"cruderasto/crud_erasto/model/models"
], function (UIComponent, Device, models) {
	"use strict";

	return UIComponent.extend("cruderasto.crud_erasto.Component", {

		metadata: {
			manifest: "json"
		},

		/**
		 * The component is initialized by UI5 automatically during the startup of the app and calls the init method once.
		 * @public
		 * @override
		 */
		 
		init: function () {
			// call the base component's init function
			UIComponent.prototype.init.apply(this, arguments);

			// enable routing
			this.getRouter().initialize();

			// set the device model
			//this.setModel(models.createDeviceModel(), "device");
			var sServiceUrl = this.getMetadata().getManifestEntry("sap.app").dataSources.Z_ERASTO_ODATA_SRV.uri;
			
			// Create and set domain model to the component.
            var oModel = new sap.ui.model.odata.ODataModel(sServiceUrl, {
            	json: true,
            	loadMetadataAsync: false
            });

			// set the device model
			this.setModel(models.createDeviceModel(), "device");
			this.setModel(oModel, "UserDetail"); 
		}
	});
});