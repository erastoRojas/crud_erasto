sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/m/MessageBox",
	"sap/ui/model/json/JSONModel"
], function (Controller, MessageBox) {
	"use strict";

	return Controller.extend("cruderasto.crud_erasto.controller.View1", {
		onInit: function () {
			var oModel = this.getView().getModel();
			// oModel.setSizeLimit(2);
			this.router = this.getOwnerComponent().getRouter();
		},

		insertarDatos: function () {
			var that = this;
			var oModel = this.getView().getModel();
			var obj = {};

			obj.Partner = this.getView().byId("partner").getValue();
			obj.NameFirst = this.getView().byId("nombre").getValue();
			obj.NameLast = this.getView().byId("apellido").getValue();
			obj.Birthpl = this.getView().byId("ciudad").getValue();

			oModel.create("/usuariosErastoSet", obj, {
				success: function (args2) {
					var hola = 1;
				}.bind(this),
				error: function (args) {
					// var modelJson1 = JSON.parse(args.responseText);
					var message = JSON.parse(args.response.body).error.message.value;
					MessageBox.error(
						message, {
							actions: [MessageBox.Action.CLOSE],
							onClose: function (sAction) {}.bind(this)
						}
					);
				}
			}); 
		},

		actualizarDatos: function () {
			var oModel = this.oView.getModel("UserDetail");
			var obj = {};
			var partner = "/usuariosErastoSet";
			var that = this;

			partner += "('" + this.getView().byId("partner").getValue() + "')";
			obj.Birthpl = this.getView().byId("ciudad").getValue();
			obj['NameLast'] = this.getView().byId("apellido").getValue();
			obj['NameFirst'] = this.getView().byId("nombre").getValue();
			obj.Partner = this.getView().byId("partner").getValue();

			oModel.update(partner, obj, {
				success: function (args2, oResponse) {
					that.getOwnerComponent().getModel().refresh();
				},
				error: function (args) {

				}
			});
		},

		borrarDatos: function () {
			var oModel = this.oView.getModel("UserDetail");
			var obj = {};
			var that = this;
			var partner = "/usuariosErastoSet";

			partner += "('" + this.getView().byId("partner").getValue() + "')";
			obj.Birthpl = this.getView().byId("ciudad").getValue();
			obj['NameLast'] = this.getView().byId("apellido").getValue();
			obj['NameFirst'] = this.getView().byId("nombre").getValue();
			obj.Partner = this.getView().byId("partner").getValue();

			oModel.remove(partner, {
				success: function (args2, oResponse) {
					that.getOwnerComponent().getModel().refresh();
				},
				error: function (args) {

				}
			});
		},

		/*Funcion que lanza Modificar anexos y hace una llamada GET para rellenar X-CSRF-Token */
		onChangeModifyEvidenceApproval: function (oEvent) {
			this._idComponentModify = oEvent.getParameters().id;
			var i18n = this.getView().getModel("i18n");
			var tituloFragmento = i18n.getProperty("info_title_subir_evidencia");
			//var fileUploader = this.getView().byId("selectionUploadEvidenceApproval");
			//fileUploader.removeAllItems(); //destroyItems();
			this._idTipoArchivo = "selectionUploadEvidenceApproval";
			var that = this;
			OData.request({
					requestUri: "/sap/opu/odata/SAP/ZCVSW_ODATA_SRV/evidenciaSet",
					method: "GET",
					headers: {
						"X-Requested-With": "XMLHttpRequest",
						"Content-Type": "application/atom+xml",
						"DataServiceVersion": "2.0",
						"X-CSRF-Token": "Fetch"
					}
				},
				function (data, response) {
					that._header_xcsrf_token = response.headers['x-csrf-token'];
					that.onChangeModifyUploadActivity(tituloFragmento);
				},
				function (data, response) {
					var i18n = that.getView().getModel("i18n");
					that._header_xcsrf_token = data.response.headers["x-csrf-token"];
					that.onChangeModifyUploadActivity(tituloFragmento);
				}
			);
		},
		
		navegarPaginaDos: function (oEvt) {
			var paramUno = this.getView().byId("ciudad").getValue();
			
			this.router.navTo("View2", {
					param: "paramUno"
				}, null);
				
		},
		
		navegarPaginaTres: function (oEvt){
			this.router.navTo("View3", {
					param: "param"
				}, null);
		}

	});
});