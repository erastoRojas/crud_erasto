sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/core/routing/History"
], function (Controller, History) {
	"use strict";

	return Controller.extend("cruderasto.crud_erasto.controller.View3", {

		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf cruderasto.crud_erasto.view.View2
		 */
		onInit: function () {

		},
		
		_onObjectMatched : function (oEvt) {
			var oArgs, oView;
			oArgs = oEvent.getParameter("arguments").param;
			oView = this.getView();
			var inpCarrId = this.byId("inpCarrName");
			// Damos el valor del parametro del routing al input
			inpCarrId.setValue(oArgs.param);
		},
		
		prueba : function (oEvent) {
			var oHistory, sPreviousHash;
			oHistory = History.getInstance();
			sPreviousHash = oHistory.getPreviousHash();
			if (sPreviousHash !== undefined) {
				window.history.go(-1);
			} else {
				this.getRouter().navTo("home", {}, true /*no history*/ );
			}
		},
		
		getRouter: function() {
			return sap.ui.core.UIComponent.getRouterFor(this);
		}
		
		
		// _onObjectMatched: function (oEvt) {
		// 	// En el pase de parametros, recogemos el valor del parametro "carrid"
		// 	var carrId = oEvt.getParameter("arguments").carrid;
		// 	var inpCarrId = this.byId("inpCarrName");
		// 	// Damos el valor del parametro del routing al input
		// 	inpCarrId.setValue(carrId);
		// },
		

		/**
		 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
		 * (NOT before the first rendering! onInit() is used for that one!).
		 * @memberOf cruderasto.crud_erasto.view.View2
		 */
		//	onBeforeRendering: function() {
		//
		//	},

		/**
		 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
		 * This hook is the same one that SAPUI5 controls get after being rendered.
		 * @memberOf cruderasto.crud_erasto.view.View2
		 */
		//	onAfterRendering: function() {
		//
		//	},

		/**
		 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
		 * @memberOf cruderasto.crud_erasto.view.View2
		 */
		//	onExit: function() {
		//
		//	}

	});

});