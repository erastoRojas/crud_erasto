/* global QUnit */
QUnit.config.autostart = false;

sap.ui.getCore().attachInit(function () {
	"use strict";

	sap.ui.require([
		"cruderasto/crud_erasto/test/integration/AllJourneys"
	], function () {
		QUnit.start();
	});
});